import React from 'react'
import {Router, Redirect} from '@reach/router'
import CourseListPage from './Pages/CourseListPage'
import CourseDetailPage from './Pages/CourseDetailPage'

const App = () => {
    return(
        <Router>
            <Redirect from="/" to="/courses"/>
            <CourseListPage path="/courses"/>
            <CourseDetailPage path="/courses/:courseId" />
        </Router>
    )
}

export default App
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {createStore} from 'redux'
import {Provider} from 'react-redux'
import Counter from './Counter';

export const INCREMENT = 'INCREMENT'
export const DECREMENT = 'DECREMENT'

export function increment(){
  return {type: 'INCREMENT'}
}

export function decrement(){
  return {type: 'DECREMENT'}
}

const initialState = {
  count: 0
}

function reducer(state = initialState, action) {
  //returns undefined
  console.log('reducer', action)
  switch(action.type){
    case INCREMENT:
      return{
        count: state.count + 1
      }
    case DECREMENT:
      return{
        count: state.count - 1
      }
    default:
      return state
  }
}

const store = createStore(reducer)
console.log('run', store.getState())
// store.dispatch({type: 'INCREMENT'})
// store.dispatch({type: 'INCREMENT'})
// store.dispatch({type: 'INCREMENT'})
// console.log('middle', store.getState())
// store.dispatch({type: 'DECREMENT'})
// console.log('done', store.getState())

const App = () => (
  <Provider store={store}>
    <Counter />
  </Provider>
)
  
ReactDOM.render(
    <App />,
  document.getElementById('root')
);

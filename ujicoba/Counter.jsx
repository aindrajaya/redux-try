import React from 'react'
import {connect} from 'react-redux'
import {increment, decrement} from './index'

class Counter extends React.Component{
    // state={
    //     count:0
    // }

    increment = () => {
        // this.setState(state => ({
        //     count: state.count + 1
        // }))
        
        //fill later
        this.props.increment()
    }

    decrement = () => {
        // this.setState(state => ({ 
        //     count : state.count - 1
        // }))

        //fill later
        this.props.decrement()
    }

    render(){
        return(
        <div className="coutner">
            <h2>Counter</h2>
            <div>
                <button onClick={this.decrement}>-</button>
                <span className="count">{this.props.count}</span>
                <button onClick={this.increment}>+</button>
            </div>
        </div>
        ) 
    }
}

function mapStateToProps(state){
    return{
        count: state.count
    }
}

const mapDispatchToProps = {
    increment,
    decrement
}

export default connect(mapStateToProps, mapDispatchToProps)(Counter)
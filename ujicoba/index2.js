import React from 'react'
import ReactDOM from 'react-dom'
import {createStore} from 'redux'
import {connect, Provider} from 'react-redux'
import flags from './Flags/Flags'
import './index2.css'

const slytherin = {
    id: 3,
    name: 'Slytherin',
    image: flags.slytherin,
    points: 0
}

const initialState = {
    selectedHouse: null,
    houses: {
        0: {
            id: 0,
            name: 'Gryffindor',
            image: flags.gryffindor,
            points: 0
        },
        1: {
            id: 1,
            name: 'Ravenclaw',
            image: flags.ravenclaw,
            points: 0
        },
        2: {
            id: 2,
            name: 'Hufflepuff',
            image: flags.hufflepuff,
            points: 0
        }
        //one is missing
    }
}

function reducer(state=initialState, action){
    switch (action.type) {
        case 'SELECT_HOUSE':
            return{
                ...state,
                selectedHouse: action.house.id
            }
        case 'ADD_POINTS':
            return{
                ...state,
                houses: {
                    ...state.houses,
                    [action.house.id]: {
                        ...state.houses[action.house.id],
                        points:
                            state.houses[action.house.id].points + 
                            action.points
                    }
                }
            }
        default:
            return state;
    }
}

const store = createStore(reducer)

function selecHouse(house){
    return{
        type: 'SELECT_HOUSE',
        house
    }
}

function addPoints(house, points){
    return{
        type: 'ADD_POINTS',
        house,
        points
    }
}

const SchoolAdmin = ({
    houses,
    selectedHouse,
    selectHouse,
    addPoints
}) => (
    <main>
        {houses.map(house => (
            <div
                key={house.id}
                onClick={() => selecHouse(house)}
                onDoubleClick={() => addPoints.house, 10}
                className = {
                    house.id === selectedHouse ? `selected ${house.name}` : ''
                }
                >
                <img src={house.image} alt={house.name}/>
                <div>{house.points} points</div>
            </div>
        ))}
    </main>
)

const mapState = state =>({
    hosues: Object.values(state.hosues),
    selectedHouse: state.selecHouse
})

const ConnectApp = connect(
    mapState, {selectHouse, addPoints}
)(SchoolAdmin)

ReactDOM.render(
    <Provider store={store}>
        <ConnectApp />
    </Provider>,
    document.querySelector('#root')
)
import {createStore} from 'redux'

const initialState = {
    switchA: false,
    switchB: false,
    light: false
}

function lightSwitchReducer(state = initialState, action){
    switch (action.type) {
        case "A_ON":
            return{
                switchA: true,
                switchB: state.switchB,
                light: true 
            }
        case "A_OFF":
            return{
                switchA: false,
                switchB: state.switchB,
                light: state.switchB 
            }
        case "B_ON":
            return{
                switchA: state.switchA,
                switchB: true,
                light: true
            }
        case "B_OFF":
            return{
                switchA: state.switchA,
                switchB: false,
                light: state.switchA
            }
        default:
            return state;
    }
}

const store = createStore(lightSwitchReducer)
console.log('initial', store.getState())
store.dispatch({case: "A_ON"})
console.log('AON', {type: "A_ON"})

